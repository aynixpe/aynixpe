<?php
class Admin extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('item_slider_model', 'usuarios_model'));
        $this->load->library('session');
    }

    public function index(){
        if(!$this->session->userdata('login'))
			redirect('admin/ingreso');

        $data = array(
			'usuario' => $this->session->userdata('usuario')
		);
		$this->load->view('item_slider', $data);
    }

    public function ingreso(){
		$this->load->view('inicio_sesion');
	}

    public function login(){
		$usuario = $this->input->post('usuario');
		$password = md5($this->input->post('password'));

		$chk_user = $this->usuarios_model->login($usuario, $password);

		if($chk_user == TRUE){
			$data = array(
				'login' => TRUE,
				'idUsuario' => $chk_user->idusuario,
				'usuario' => $chk_user->usuario
			);
			$this->session->set_userdata($data);
			echo json_encode(true);
		}else
			echo json_encode(false);
	}

	public function logout()		{
		$this->session->sess_destroy();
		$this->ingreso();
	}

    public function insert_item(){
        if(!$this->session->userdata('login'))
			redirect('admin/ingreso');
        $idusuario = $this->session->userdata('idUsuario');
        $orden = 1;
        $titulo = $this->input->post('titulo');
        $descripcion = $this->input->post('descripcion');
        $estado = 'A';

        $file_element_name = 'imagen';

        $config['upload_path'] = './files/';
        $config['allowed_types'] = 'gif|jpg|png|doc|txt';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $data = array();
        if (!$this->upload->do_upload($file_element_name)){
            $data['state'] = false;
            $data['error']['code'] = 2;
            $data['error']['message'] = $this->upload->display_errors('', '');
        }else{
            $img = $this->upload->data();
            $url = $img['file_name'];
            $data['state'] = $this->item_slider_model->insert($orden, $titulo, $descripcion, $url, $idusuario, $estado);
        }

        if(!$data['state']){
            if(!isset($data['error']))
                $data['error'] = unserialize(ERR_CONEXION);
            if(isset($img))
                unlink($img['full_path']);
        }

        @unlink($_FILES[$file_element_name]);


        echo json_encode($data);
    }

    public function get_all(){
        echo json_encode($this->item_slider_model->get_all(0));
    }
}
