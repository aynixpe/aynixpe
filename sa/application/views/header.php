<div class='container-fluid hidden-xs'>
	<div class='social-menu'>
		<span class="fa-stack fa-lg">
			<i class="fa fa-circle fa-stack-2x  fa-inverse"></i>
			<i class="fa fa-twitter fa-stack-1x"></i>
		</span>
		<span class="fa-stack fa-lg">
			<i class="fa fa-circle fa-stack-2x  fa-inverse"></i>
			<i class="fa fa-facebook fa-stack-1x"></i>
		</span>
		<span class="fa-stack fa-lg">
			<i class="fa fa-circle fa-stack-2x  fa-inverse"></i>
			<i class="fa fa-youtube fa-stack-1x"></i>
		</span>
		<span class="fa-stack fa-lg">
			<i class="fa fa-circle fa-stack-2x  fa-inverse"></i>
			<i class="fa fa-google-plus fa-stack-1x"></i>
		</span>
	</div>
</div>
<nav class='navbar navbar-default navbar-static-top'>
	<div class="container-fluid">
	    <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				<img src='<?php echo base_url();?>img/logo.svg' class='img-brand' />
			</a>
		</div>
		<div class="collapse navbar-collapse" id="menu">
			<ul class="nav navbar-nav navbar-right">
				<li id='mnu-inicio' class='mnu-border-right'><a href='<?php echo base_url(); ?>inicio'>INICIO</a></li>
				<li id='mnu-quienes-somos' class='mnu-border-right'><a href='<?php echo base_url(); ?>#'>QUIENES SOMOS</a></li>
				<li id='mnu-servicio' class='mnu-border-right'><a href='<?php echo base_url(); ?>#'>SERVICIO</a></li>
				<li id='mnu-contactanos' class='mnu-border-right'><a href='<?php echo base_url(); ?>#'>CONTACTANOS</a></li>
				<li id='mnu-buscar'><a href='<?php echo base_url(); ?>#'><i class='fa fa-search'></i></a></li>
				<?php
				if(isset($usuario)){
					echo "<li class='dropdown'>";
					echo "	<a href='#' class='dropdown-toggle mnu-usuario' data-toggle='dropdown' role='button' aria-expanded='false'> $usuario<span class='caret'></span></a>";
					echo "	<ul class='dropdown-menu' role='menu'>";
					echo "		<li><a href='".base_url()."admin/logout'>Salir</a></li>";
					echo "	</ul>";
					echo "</li>";
				}
				?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class='container-fluid hidden-xs'>
	<div class='mnu-info pull-right vcenter'>
		<i class='fa fa-phone fa-2x'></i>&nbsp;&nbsp;
		<span class='text-left'>Te asesoramos (+51)015294259</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<i class='fa fa-map-marker fa-2x'></i>&nbsp;&nbsp;
		<span class='text-left'>Calle Federico Bulne Dpto 301 - Lima</span>
	</div>
</div>
