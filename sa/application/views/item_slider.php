<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Suramerica Express Cargo</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/jasny-bootstrap.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
</head>
<body>
    <header class='container-fluid'>
        <?php $this->load->view('header', array($usuario)); ?>
    </header>
    <section class='container-fluid'>
		<div class='row' style='background-color: #EBEBEB;'>
			<div class='col-xs-12'>
				<h2>Portada
					<button type='button' id='btn-nuevo' class='btn btn-primary pull-right'><i class='fa fa-plus'></i> Nuevo </button>
				</h2>
			</div>
		</div>
		<div class='row cuadros-container' style='background-color: #EBEBEB;' id='lst-items'>

		</div>
    </section>
	<!-- MODALES               -->
	<div class="modal fade" id='mdl-gestion-item'>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Ítem de portada</h4>
				</div>
				<div class="modal-body">
					<?php $this->load->view('frm_gestion_item'); ?>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- FIN MODALES           -->
    <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/jasny-bootstrap.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
    <script type='text/javascript'>
		var base_url = '<?php echo base_url(); ?>';
        $(document).ready(function(e){
			cargar_items();
			$('#fgi-btn-cancelar').on('click', function(e){
				$('#mdl-gestion-item').modal('hide');
			});
			$('#mdl-gestion-item').on('hide.bs.modal', function(e){
				cargar_items();
			});
			$('#frm-gestion-item').on('submit', function(e){
				e.preventDefault();
				$.ajax({
					url: base_url + 'admin/insert_item',
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData: false,
					type: 'post',
					dataType: 'json',
					success: function(data){
						var html = "";
						if(data.state){
							html += "<div class='alert alert-success alert-dismissible fade in' role='alert'>";
							html += "	<button class='close' aria-label='Close' data-dismiss='alert' type='button'>";
							html += "		<span aria-hidden='true'>&times;</span>";
							html += "	</button>";
							html += "	<h4>";
							html += "		Éxito";
							html += "		<a class='anchorjs-link' href='#'></a>";
							html += "	</h4>";
							html += "	<p>";
							html += "		Información cargada con éxito";
							html += "	</p>";
							html += "</div>";
						}else{
							html += "<div class='alert alert-danger alert-dismissible fade in' role='alert'>";
							html += "	<button class='close' aria-label='Close' data-dismiss='alert' type='button'>";
							html += "		<span aria-hidden='true'>&times;</span>";
							html += "	</button>";
							html += "	<h4>";
							html += "Error"
							html += "		<a class='anchorjs-link' href='#'></a>";
							html += "	</h4>";
							html += "	<p>";
							html += data.error['message'];
							html += "	</p>";
							html += "</div>";
						}
						$('#fgi-alert').html(html);
					}
				});
			});
			$('#btn-nuevo').on('click', function(e){
				e.preventDefault();
				$('#mdl-gestion-item').modal('show');
			});
        });
		function cargar_items(){
			get_items(function(data){
				var html = "";
				for(var i = 0; i < data.length; i++){
					if(i%3 == 0){
						html += "<div class='container-fluid'><div class='row'>";
					}
					html += "<div class='col-sm-4'>";
					html += "	<div class='bg-blanco cuadro'>";
					html += "		<div class='container-fluid'>";
					html += "			<div class='row'>";
					html += "				<div class='col-xs-12'>";
					html += "					<img src='<?php echo base_url();?>files/" + data[i]['url'] + "' class='img-responsive' />";
					html += "				</div>";
					html += "			</div>";
					html += "			<div class='row'>";
					html += "				<div class='col-xs-12'>";
					html += "					<h3>" + data[i]['titulo'];
					html += "						<button type='button' class='btn btn-primary btn-sm pull-right'><i class='fa fa-pencil'></i></button>";
					html += "					</h3>";
					html += "					<p>" + data[i]['descripcion'] + "</p>";
					html += "					<div class='checkbox'>";
					html += "					<label>";
					html += "<input type='checkbox' ";
					html += (data[i]['estado'] == 'A') ? 'checked' : '';
					html += ">";
					html += "						Activo";
					html += "					</label></div>";
					html += "				</div>";
					html += "			</div>";
					html += "		</div>";
					html += "	</div>";
					html += "</div>";
					if(i%3 == 2){
						html += "</div></div>";
					}
				}
				$('#lst-items').html(html);
			});
		}
    </script>
</body>
</html>
