<!DOCTYPE html>
<html lang='es'>
  <head>
    <meta charset='utf-8' />
    <meta name='description' content='Aynix ofrece servicios de Social Media, Diseño web, Desarrollo de Aplicaciones Móviles y Sistemas Empresariales, Diseño Gráfico y Fotografía.' />
    <meta name='keywords' content='Aynix, Social, Media, Web, Diseño, Desarrollo, Movil, Fotografia, Agencia, Marketing, Clientes, Plataformas, Chiclayo, Peru' />
    <meta name='author' content='Aynix S.A.C.' />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Aynix - Believe, Create</title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
    <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css' />
    <link rel='stylesheet' type='text/css' href='css/font-awesome.css' />
    <link rel='stylesheet' type='text/css' href='css/preload.css' />
    <link rel='stylesheet' type='text/css' href='css/main.css' />
</head>
<body data-spy="scroll" data-target=".ubicar">
    <div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
    <!--div id='skrollr-body'-->
        <nav class='navbar navbar-inverse navbar-fixed-top'>
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class='navbar-toggle collapsed btn-menu' type="button" data-toggle='collapse' data-target='#menu' >
                        <span class='fa-stack fa-lg'>
                            <i class='fa fa-stop fa-stack-2x'></i>
                            <i class='fa fa-bars fa-stack-1x fa-inverse'></i>
                        </span>
                    </button>
                    <a class="navbar-brand" href="#inicio">
                        <img alt='Brand' src="img/logo.png" alt="">
                    </a>
                </div>
                <div class='collapse navbar-collapse ubicar' id='menu'>
                    <ul class='nav navbar-nav navbar-right menu-ancho'>
                        <li><a class="verde" href="#quienes-somos"><i class='hidden-sm hidden-md hidden-lg fa fa-users'></i> ¿Quiénes Somos?</a></li>
                        <li class='divider hidden-xs'><i class='fa fa-circle aynix-naranja'></i></li>
                        <li><a class="naranja" href="#servicios"><i class='hidden-sm hidden-md hidden-lg fa fa-desktop'></i> Servicios</a></li>
                        <li class='divider hidden-xs'><i class='fa fa-circle aynix-rojo'></i></li>
                        <li><a class="rojo" href="#clientes"><i class='hidden-sm hidden-md hidden-lg fa fa-shopping-cart'></i> Clientes</a></li>
                        <li class='divider hidden-xs'><i class='fa fa-circle aynix-azul'></i></li>
                        <li><a class="azul" href="#contactanos"><i class='hidden-sm hidden-md hidden-lg fa fa-envelope-o'></i> Contáctanos</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <section id='inicio'>
            <div class='container full-v'>
                <div class="row full-v fondo contenedor-img">
                    <img src='img/principal_2.svg' class='img-responsive hidden-xs' id='img-animacion'>
                    <img src='img/principal_3.svg' class='img-responsive visible-xs' id='img-animacion'>
                    <img src='img/elem_nubes.svg' class='img-responsive obj-animacion obj-anim-nube' id='obj-anim-nube-1'/>
                    <img src='img/elem_nubes_1.svg' class='img-responsive obj-animacion obj-anim-nube' id='obj-anim-nube-2'/>
                    <img src='img/elem_nubes.svg' class='img-responsive obj-animacion obj-anim-nube' id='obj-anim-nube-3'/>
                    <img src='img/carrito2.svg' class='img-responsive obj-animacion obj-anim-carro' id='obj-anim-carro-1'/>
                    <img src='img/carrito1.svg' class='img-responsive obj-animacion obj-anim-carro' id='obj-anim-carro-2'/>
                    <img src='img/motito.svg' class='img-responsive obj-animacion obj-anim-moto' id='obj-anim-moto-1'/>
                    <img src='img/logo_blanco.png' class='img-responsive obj-animacion obj-logo-banner'/>
                    <img src='img/globo.svg' class='img-responsive obj-animacion obj-anim-globo'/>
                    <img src='img/arbol.svg' class='img-responsive obj-animacion obj-arbol' id='obj-anim-arbol-1'/>
                    <img src='img/arbol.svg' class='img-responsive obj-animacion obj-arbol' id='obj-anim-arbol-2'/>
                    <img src='img/arbol.svg' class='img-responsive obj-animacion obj-arbol' id='obj-anim-arbol-3'/>
                    <img src='img/arbol.svg' class='img-responsive obj-animacion obj-arbol' id='obj-anim-arbol-4'/>
                    <img src='img/arbol.svg' class='img-responsive obj-animacion obj-arbol' id='obj-anim-arbol-5'/>
                    <img src='img/senial_wifi.svg' class='img-responsive obj-animacion' id='obj-senial-wifi'/>
                    <a href='#servicios'><img src='img/burbuja_1.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-1' /></a>
                    <a href='#servicios'><img src='img/burbuja_2.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-2' /></a>
                    <a href='#servicios'><img src='img/burbuja_3.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-3' /></a>
                    <a href='#servicios'><img src='img/burbuja_4.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-4' /></a>
                    <a href='#servicios'><img src='img/burbuja_5.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-5' /></a>
                    <a href='#servicios'><img src='img/burbuja_6.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-6' /></a>
                    <a href='#servicios'><img src='img/burbuja_7.svg' class='img-responsive obj-animacion obj-burbuja' id='obj-burbuja-7' /></a>
                    <div class='frases text-center'>
                        <p class='lead'>"Aynix, ayudando a que tu negocio crezca"</p>
                    </div>
                </div>
            </div>
        </section>
        <section id='quienes-somos'>
            <div class='container'>
                <div class='row fondo flex'>
                    <div class='col-sm-5'>
                        <div class='container'>
                            <div class='row'>
                                <div class='col-xs-12'>
                                    <img src='img/flechas_top.svg' style='width: 100%' />
                                </div>
                            </div>
                            <div class='row flex'>
                                <div class='visible-lg col-lg-4 '>
                                    <img src='img/flechas_left.svg' style='width: 100%;'/>
                                </div>
                                <div class='col-lg-8'
                                data-450-top="transform: translateX(-150%);"
                                data-100-top="transform: translateX(0%);"
                                data-anchor-target="#quienes-somos">
                                    <div class='text-center info' >
                                        <h2 style='margin-top:0px;'><b>¿Quiénes somos?</b></h2>
                                        <p class='lead'>
                                            Somos una compañía dedicada<br>a los Servicios Digitales.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class='row hidden-xs'>
                                <div class='col-xs-12'>
                                    <img src='img/flechas_bottom.svg' style='width: 100%' />
                                </div>
                            </div>
                    </div>
                        </div>
                    <div class="col-sm-7 col-xs-12">
                        <div class='container'>
                            <div class='row flex'>
                                <div class='col-xs-12 col-md-9 cont-img-tablet'
                                data-450-top="transform: translateX(150%);"
                                data-100-top="transform: translateX(0%);"
                                data-anchor-target="#quienes-somos">
                                    <img id="img-tablet" class='img-responsive' src="img/tablet_2.svg" alt="tablet-video">
                                    <iframe id='video' src="https://www.youtube.com/embed/bJFLpzKVmO0" frameborder="0" allowfullscreen></iframe>
                                    <!--video id='video' controls >
                                        <source src='video/portada.mp4' type='video/mp4'/>
                                        <source src='video/portada.webm' type='video/webm' />
                                    </video-->
                                </div>
                                <div class='col-md-3 hidden-sm hidden-xs'>
                                    <img src='img/flechas_right.svg' style='width:100%;'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="servicios">
            <div class="row col-condensed">
                <div class="col-sm-4 cuadro">
                    <div class='row col-condensed k-effect rotate'>
                        <div class="col-xs-12 cuadro">
                            <img class="full-h" src="img/imagen_1.jpg" alt=""><span class="mask mask1"></span>
                        </div>
                        <div class="col-xs-6 cuadro img2">
                            <img class="full-h" src="img/imagen_2.png"alt=""><span class="mask mask2"></span>
                        </div>
                        <div class="col-xs-6 cuadro img2">
                            <img class="full-h" src="img/imagen_3.jpg"alt=""><span class="mask mask3"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 cuadro">
                    <div class="k-effect rotate">
                        <div>
                            <img class="full-h" src="img/imagen_4.jpg" alt=""><span class="mask mask4"></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 cuadro">
                    <div class='row col-condensed'>
                        <div class='col-xs-6 cuadro k-effect rotate'>
                            <div class='row col-condensed'>
                                <div class='col-xs-12 cuadro'>
                                    <img class="full-h" src="img/imagen_5.jpg" alt=""><span class="mask mask5"></span>
                                </div>
                            </div>
                            <div class='row col-condensed'>
                                <div class='col-xs-12 cuadro'>
                                    <img class="full-h" src="img/imagen_6.jpg" alt=""><span class="mask mask6"></span>
                                </div>
                            </div>
                        </div>
                        <div class='col-xs-6 cuadro k-effect rotate'>
                            <div>
                                <img class="full-h" src="img/imagen_7.jpg" alt=""><span class="mask mask7"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id='clientes'>
            <div class='container'>
                <div class='row fondo flex'>
                    <div class='col-sm-7'>
                        <div class='row'>
                            <div class='col-xs-12'>
                                <div class='col-sm-offset-2 col-sm-10 info'>
                                    <h2 style='margin-top: 0px;'>Nuestros Clientes</h2>
                                    <p class='lead text-justify'>Ayudamos a Pequeñas y Medianas Empresas pensando y diseñando estrategias para su crecimiento.<br>
                                    Si quieres saber más sobre lo que hacemos y cómo podemos ayudar a que tus clientes sonrían... <b>¡Suscríbete!</b></p>
                                    <form id='frm-suscripcion'>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary sin-radius" id='btnSuscripcion' type="submit">Suscribirse</button>
                                            </span>
                                            <input type="text" class="form-control sin-radius" id='txtSuscripcion' placeholder="Correo ...">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class='container'>
                            <div class='visible-xs visible-sm' style='height:30px;'>
                            </div>
                            <div class='row flex'>
                                <div class='col-xs-6 col-sm-5'>
                                    <img class='img-responsive center-block' src='img/eden.fw.png' />
                                </div>
                                <div class='col-xs-6 col-sm-5'>
                                    <img class='img-responsive center-block' src='img/ankdes.png' />
                                </div>
                            </div>
                            <div class='row flex'>
                                <div class='col-md-offset-2 col-xs-6 col-sm-5'>
                                    <img class='img-responsive center-block' src='img/closet.png' />
                                </div>
                                <div class='col-xs-6 col-sm-5'>
                                    <img class='img-responsive center-block' src='img/skina.png' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="contactanos">
            <div class='container'>
                <div class='row fondo'>
                    <div class='col-sm-offset-2 col-sm-8'>
                        <h2 class='text-center' style='margin-top: 0;'>Contáctanos</h2>
                        <form class='form-horizontal col-condensed' id='frm-contacto'>
                            <div class='col-md-6'>
                                <div class='form-group'>
                                    <label class='col-xs-3 col-md-2 control-label'>Nombre</label>
                                    <div class='col-xs-9 col-md-10'>
                                        <input type='text' id='txtNombre' class='form-control input-sm sin-radius' required/>
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-6'>
                                <div class='form-group'>
                                    <label class='col-xs-3 col-md-2 control-label'>Correo</label>
                                    <div class='col-xs-9 col-md-10'>
                                        <input type='email' id='txtCorreo' class='form-control input-sm sin-radius' required/>
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-6'>
                                <div class='form-group'>
                                    <label class='col-xs-3 col-md-2 control-label'>Empresa</label>
                                    <div class='col-xs-9 col-md-10'>
                                        <input type='text' id='txtEmpresa' class='form-control input-sm sin-radius' required/>
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-6'>
                                <div class='form-group'>
                                    <label class='col-xs-3 col-md-2 control-label'>Teléfono</label>
                                    <div class='col-xs-9 col-md-10'>
                                        <input type='text' id='txtTelefono' class='form-control input-sm sin-radius'/>
                                    </div>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='form-group'>
                                    <label class='col-xs-3 col-md-1 control-label'>Consulta</label>
                                    <div class='col-xs-9 col-md-11'>
                                        <textarea class="form-control sin-radius" id='txtConsulta' rows="3" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type='submit' class='btn btn-lg btn-primary sin-radius'>Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section id='fotos'>
            <div class='row col-condensed'>
                <div class='col-sm-4 cuadro'>
                    <div class='row col-condensed'>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_1.jpg' />
                        </div>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_2.jpg' style='transform: scaleX(-1);' />
                        </div>
                    </div>
                </div>
                <div class='col-sm-4 cuadro'>
                    <div class='row col-condensed'>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_3.jpg' />
                        </div>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_4.jpg' />
                        </div>
                    </div>
                </div>
                <div class='col-sm-4 cuadro'>
                    <div class='row col-condensed'>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_5.jpg' />
                        </div>
                        <div class='col-xs-6 cuadro'>
                            <img class='full-h' src='img/footer_img_6.jpg' />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="foot">
            <img src='img/linea.svg' />
            <div class='container'>
                <div class='row'>
                    <div class="col-xs-12">
                        <div class='container'>
                            <div class='row footer-container'>
                                <div class="col-xs-6 col-md-3">
                                    <strong>Acerca de</strong><br>
                                    Somos una empresa orientada a los servicios digitales, contamos con los mejores especialistas para ayudarte con estrategias tecnológicas que harán crecer tu negocio.
                                </div>
                                <div class="col-xs-6 col-md-3 ">
                                    <strong>Servicios</strong><br>
                                    <h5><a href="#">>Diseño Web</a></h5>
                                    <h5><a href="#">>Apps Móviles</a></h5>
                                    <h5><a href="#">>Tiendas virtuales</a></h5>
                                    <h5><a href="#">>Sistemas Empresariales</a></h5>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <strong>Síguenos</strong><br>
                                    <a href="https://www.facebook.com/aynixpe" target="_blank">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <!--i class="img-circle link link-fb fa fa-facebook"></i-->
                                    </a>
                                    <a href="https://www.twitter.com/aynixpe" target="_blank">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <!--i class="img-circle link link-twi fa fa-twitter"></i-->
                                    </a>
                                    <a href="https://www.google.com/aynixpe" target="_blank">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <!--i class="img-circle link link-gp fa fa-google-plus"></i-->
                                    </a>
                                    <a href="https://instagram.com/aynixpe" target="_blank">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <!--i class="img-circle link link-ins fa fa-instagram"></i-->
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-3 ">
                                    <address>
                                        <strong>Aynix</strong><br>
                                        Elias Aguirre #175 - Reque<br>
                                        <a href="http://www.aynix.pe">www.aynix.pe</a><br>
                                        <a href="mailto:info@aynix.pe" target="_top">info@aynix.pe</a><br>
                                        <i class="fa fa-phone"></i> (+51)957408974
                                    </address>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="col-xs-12 text-center derechos"><br>
                                    <h5>Todos los derechos reservados. @Aynix 2015</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--footer class='foot'>
            <div class='row col-condensed'>
                <div class='row'>
                    <div class='col-xs-12'>
                        <img class='full-h division' src='img/linea.svg' />
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="col-xs-6">
                        <address>
                            <strong>Aynix</strong><br>
                            Elias Aguirre #175 - Reque<br>
                            <a href="http://www.aynix.pe">www.aynix.pe</a><br>
                            <a href="mailto:info@aynix.pe" target="_top">info@aynix.pe</a><br>
                            <i class='fa fa-phone'></i> (+51)957408974
                        </address>
                    </div>
                    <div class="col-xs-6">
                        <div class='pull-right'>
                            <a href="https://www.facebook.com/aynixpe" target='_blank'>
                                <span class='fa-stack fa-lg'>
                                    <i class='fa fa-circle fa-stack-2x'></i>
                                    <i class='fa fa-facebook fa-stack-1x fa-inverse'></i>
                                </span>
                                <!--i class="img-circle link link-fb fa fa-facebook"></i>
                            </a>
                            <a href="https://www.twitter.com/aynixpe" target='_blank'>
                                <span class='fa-stack fa-lg'>
                                    <i class='fa fa-circle fa-stack-2x'></i>
                                    <i class='fa fa-twitter fa-stack-1x fa-inverse'></i>
                                </span>
                                <!--i class="img-circle link link-twi fa fa-twitter"></i>
                            </a>
                            <a href="https://www.google.com/aynixpe" target='_blank'>
                                <span class='fa-stack fa-lg'>
                                    <i class='fa fa-circle fa-stack-2x'></i>
                                    <i class='fa fa-google-plus fa-stack-1x fa-inverse'></i>
                                </span>
                                <!--i class="img-circle link link-gp fa fa-google-plus"></i>
                            </a>
                            <a href="https://instagram.com/aynixpe" target='_blank'>
                                <span class='fa-stack fa-lg'>
                                    <i class='fa fa-circle fa-stack-2x'></i>
                                    <i class='fa fa-instagram fa-stack-1x fa-inverse'></i>
                                </span>
                                <!--i class="img-circle link link-ins fa fa-instagram"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 text-right derechos">
                        <h5>Todos los derechos reservados. @Aynix 2015</h5>
                    </div>
                </div>
            </div>
        </footer>
        <!-- MODALES -->
        <div class="modal fade" id='mdlMensaje'>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id='mdlMensaje-title'></h4>
                    </div>
                    <div class="modal-body">
                        <p id='mdlMensaje-mensaje'></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    <!--/div-->
    <script type="text/javascript" src='js/jquery-1.11.1.min.js'></script>
    <script type='text/javascript' src='js/bootstrap.js'></script>
    <script src="js/skrollr.js"></script>
    <script src="js/imagesloaded.js"></script>
    <script src="js/main.js"></script>
    <script type='text/javascript'>
        $(document).ready(function() {
            setTimeout(function(){
        		$('body').addClass('loaded');
        		$('h1').css('color','#222222');
        	}, 3000);
            $('a[href*=#]').bind('click', function(e) {
                e.preventDefault();

                var target = $(this).attr("href");
                $('html, body').stop().animate({ scrollTop: $(target).offset().top}, 1100, function() {
                    location.hash = target;
                });
                return false;
            });
            mover1();
            mover1_burbuja();
            mover2();
            mover3();
            mover3_burbuja();
            mover_globo();
            mover_nube_1();
            mover_nube_2();
            mover_nube_3();
            var burbujas = $('.burbuja');
            cargarBurbujas(burbujas, 0);
            senial_wifi();

            /** ajax**/
            frm_contacto();
            frm_suscripcion();

        });

        function frm_suscripcion(){
            $('#frm-suscripcion').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    url: 'suscripcion.php',
                    type:'post',
                    datatype:'json',
                    data:{
                        email: $('#txtSuscripcion').val()
                    },
                    success: function(result){
                        var result = JSON.parse(result);
                        var title = "";
                        var mensaje = "";
                        if(result){
                            title = "Suscripción completa";
                            mensaje = "Estaremos enviándote información de nuestros paquetes y ofertas que más te puedan interesar";
                        }else{
                            title = "¡Lo sentimos!";
                            mensaje = "Ha ocurrido un error y no se ha podido concretar la suscripción, por favor recargue la página y/o intente nuevamente en unos minutos";
                        }
                        $('#mdlMensaje-title').text(title);
                        $('#mdlMensaje-mensaje').text(mensaje);
                        $('#mdlMensaje').modal('show');
                    }
                });
            });
        }

        function frm_contacto(){
            $('#frm-contacto').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    url:'mail.php',
                    type:'post',
                    datatype:'json',
                    data:{
                        txtNombre: $('#txtNombre').val(),
                        txtCorreo: $('#txtCorreo').val(),
                        txtEmpresa: $('#txtEmpresa').val(),
                        txtTelefono: $('#txtTelefono').val(),
                        txtConsulta: $('#txtConsulta').val()
                    },
                    success: function(result){
                        var result = JSON.parse(result);
                        var title = "";
                        var mensaje = "";
                        if(result){
                            title = "Enviado";
                            mensaje = "Su consulta ha sido enviada con éxito, estaremos respondiendo lo más pronto posible, ¡Esté alerta!";
                        }else{
                            title = "¡Lo sentimos!";
                            mensaje = "Ha ocurrido un error y su consulta no ha sido enviada, por favor recargue la página y/o intente nuevamente en unos minutos";
                        }
                        $('#mdlMensaje-title').text(title);
                        $('#mdlMensaje-mensaje').text(mensaje);
                        $('#mdlMensaje').modal('show');
                    }
                });
            });
        }

        function cargarBurbujas(burbujas, i){
            if(i <= 6){
                $(burbujas[i]).fadeIn(1000, 'swing', cargarBurbujas(burbujas, i+1));
            }
        }

        function mover1(){
            $('#obj-anim-carro-2').animate({
                'left': '100%'
            }, 9000, 'linear');
            $('#obj-anim-carro-2').animate({
                'left': '-12%'
            },0, 'linear',mover1);
        }
        function mover1_burbuja(){
            $('#obj-burbuja-6').animate({
                'left': '106%'
            }, 9000, 'linear');
            $('#obj-burbuja-6').animate({
                'left': '-6%'
            },0, 'linear',mover1_burbuja);
        }
        function mover2(){
            $('#obj-anim-carro-1').animate({
                'left': '-12%'
            }, 7000, 'linear');
            $('#obj-anim-carro-1').animate({
                'left': '100%'
            },0, 'linear', mover2);
        }
        function mover3(){
            $('#obj-anim-moto-1').animate({
                'left': '100%'
            }, 8000,'linear');
            $('#obj-anim-moto-1').animate({
                'left': '-5%'
            },0, 'linear', mover3);
        }
        function mover3_burbuja(){
            $('#obj-burbuja-7').animate({
                'left': '102%'
            }, 8000, 'linear');
            $('#obj-burbuja-7').animate({
                'left': '-3%'
            },0, 'linear',mover3_burbuja);
        }
        function mover_globo(){
            $('.obj-anim-globo').animate({
                'left': '-34%'
            }, 15000, 'linear');
            $('.obj-anim-globo').animate({
                'left': '100%'
            },0, 'linear', mover_globo);
        }
        function mover_nube_1(){
            $('#obj-anim-nube-1').animate({
                'left': '100%'
            }, 35000, 'linear');
            $('#obj-anim-nube-1').animate({
                'left': '-11%'
            },0, 'linear', mover_nube_1);
        }
        function mover_nube_2(){
            $('#obj-anim-nube-2').animate({
                'left': '-11%'
            }, 30000, 'linear');
            $('#obj-anim-nube-2').animate({
                'left': '100%'
            },0, 'linear', mover_nube_2);
        }
        function mover_nube_3(){
            $('#obj-anim-nube-3').animate({
                'left': '100%'
            }, 45000, 'linear');
            $('#obj-anim-nube-').animate({
                'left': '-11%'
            },0, 'linear', mover_nube_3);
        }
        function senial_wifi(){
            $('#obj-senial-wifi').fadeIn(300).delay(150).fadeOut(300, senial_wifi);
        }
    </script>
</body>
</html>
